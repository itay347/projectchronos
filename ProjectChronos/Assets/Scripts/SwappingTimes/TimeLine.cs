﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TimeLineZone
{
    past,
    present
};

public static class TimeLine
{
    private static AudioSource audioSource;
    private static ParticleSystem particleSystem;

    public delegate void SwitchTimeLine();
    public static event SwitchTimeLine onTimeSwitch;
    public static TimeLineZone currentTimeLine = TimeLineZone.past;

    public static void changeTimeLine(TimeLineZone timeLine)
    {
        TimeLine.currentTimeLine = timeLine;

        // Play a sound
        if (audioSource != null)
        {
            audioSource.PlayOneShot(audioSource.clip);
        }
        else
        {
            Debug.LogWarning("TimeLine: An AudioSource need to be set");
        }

        // Show a "teleport" particle effect
        if (particleSystem != null)
        {
            particleSystem.Play();
        }
        else
        {
            Debug.LogWarning("TimeLine: A ParticleSystem need to be set");
        }

        Debug.Log("Time changed to: " + TimeLine.currentTimeLine);
        onTimeSwitch?.Invoke();
    }

    public static void SetAudioSource(AudioSource audioSource)
    {
        TimeLine.audioSource = audioSource;
    }

    public static void SetParticleSystem(ParticleSystem particleSystem)
    {
        TimeLine.particleSystem = particleSystem;
    }
}
