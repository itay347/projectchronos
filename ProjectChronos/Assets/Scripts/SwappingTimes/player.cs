﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string eventName;
    public FMOD.Studio.EventInstance eventAudio;
    public FMOD.Studio.ParameterInstance eventParameter;

    public AudioSource audioSource;
    public ParticleSystem particleSystem;
    public TimeLineZone initialTime;

    void Start()
    {
        //DontDestroyOnLoad(gameObject);

        eventAudio = FMODUnity.RuntimeManager.CreateInstance(eventName);
        eventAudio.getParameter("MusicTime", out eventParameter);
        eventAudio.start();

        TimeLine.SetAudioSource(audioSource);
        TimeLine.SetParticleSystem(particleSystem);
        TimeLine.changeTimeLine(initialTime);
    }

    void OnDestroy()
    {
        eventAudio.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
    }

    void Update()
    {
        if (Input.GetButtonDown("SwitchTime"))
        {
            if (TimeLine.currentTimeLine == TimeLineZone.past)
            {
                eventParameter.setValue(1);
                TimeLine.changeTimeLine(TimeLineZone.present);
            }
            else
            {
                eventParameter.setValue(0);
                TimeLine.changeTimeLine(TimeLineZone.past);
            }
        }
    }
}
