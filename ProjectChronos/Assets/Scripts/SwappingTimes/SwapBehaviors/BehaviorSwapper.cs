﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class BehaviorSwapper : MonoBehaviour
{
    Dictionary<TimeLineZone, Action> timeSwaper;

    void Awake()
    {
        TimeLine.onTimeSwitch += SwapTime;
        timeSwaper = new Dictionary<TimeLineZone, Action>();
        timeSwaper.Add(TimeLineZone.past, PastSwitch);
        timeSwaper.Add(TimeLineZone.present, PresentSwitch);
        //DontDestroyOnLoad(this.gameObject);
    }

    void OnDestroy()
    {
        TimeLine.onTimeSwitch -= SwapTime;
    }

    public abstract void PastSwitch();

    public abstract void PresentSwitch();

    void SwapTime()
    {
        timeSwaper[TimeLine.currentTimeLine]();
    }
}
