﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Visibillity
{
    public TimeLineZone timeZone;
    public bool visible;
}
public class VisibillitySwapper : BehaviorSwapper
{

    public Visibillity[] timeVisibillity = new Visibillity[2];

    public override void PastSwitch()
    {
        SetVisibillity(GetComponent<Collider2D>(), GetComponent<SpriteRenderer>(), timeVisibillity[(int)TimeLineZone.past]);
    }

    public override void PresentSwitch()
    {
        SetVisibillity(GetComponent<Collider2D>(), GetComponent<SpriteRenderer>(), timeVisibillity[(int)TimeLineZone.present]);
    }

    private void SetVisibillity(Collider2D objectCollider, Renderer objectRenderer, Visibillity toUse)
    {
        if (objectCollider != null)
            objectCollider.enabled = toUse.visible;
        if (objectRenderer != null)
            objectRenderer.enabled = toUse.visible;

        if (GetComponent<Rigidbody2D>() != null)
            if (!toUse.visible)
                GetComponent<Rigidbody2D>().Sleep();
            else
                GetComponent<Rigidbody2D>().WakeUp();
    }
}
