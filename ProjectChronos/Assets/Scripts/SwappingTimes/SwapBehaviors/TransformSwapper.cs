﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TransformHolder
{
    public Vector3 position;
    public Vector3 rotation;
    public Vector3 scale;
}

public class TransformSwapper : BehaviorSwapper
{
    public TransformHolder pastTransform;
    public TransformHolder presentTransform;

    public override void PastSwitch()
    {
        SetTransform(GetComponent<Transform>(), pastTransform);
    }

    public override void PresentSwitch()
    {
        SetTransform(GetComponent<Transform>(), presentTransform);
    }

    private static void SetTransform(Transform toSet, TransformHolder toUse)
    {
        toSet.localPosition = toUse.position;
        toSet.localRotation = Quaternion.Euler(toUse.rotation.x, toUse.rotation.y, toUse.rotation.z);
        toSet.localScale = toUse.scale;
    }
}
