﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class Stages : MonoBehaviour
{
    public GameObject buttonPrefab;
    public float space = 1.1f;

    void Start()
    {
        buttonPrefab.GetComponent<Button>().onClick.AddListener(() => StageManager.LoadStageById(1));
        MyAwesomeCreator();
    }

    public void BackToTheLobby()
    {
        SceneManager.LoadScene("loby");
    }

    void MyAwesomeCreator()
    {
        
        Vector3 offset = new Vector3(space, 0, 0);
        
        for (int i = 2; i <= StageManager.amountOfStages; i++)
        {
            GameObject goButton = Instantiate(buttonPrefab);
            goButton.transform.SetParent(GameObject.Find("Canvas").GetComponent<Transform>(), false);
            goButton.GetComponent<Transform>().position += offset;
            offset.x += space;

            int stageNumber = i;
            goButton.GetComponentInChildren<Text>().text = stageNumber.ToString();
            //if(!StageManager.StageDone(stageNumber))
            //{
            //    goButton.GetComponent<Button>().interactable = false;
            //}
            goButton.GetComponent<Button>().onClick.AddListener(() => StageManager.LoadStageById(stageNumber));
        }
    }
}
