﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class StageManager
{
    public static int amountOfStages = 3;
    private static bool[] stagesDone = new bool[amountOfStages];
    private static string stagePrefix = "Stage";
    private static int currentStage = 1;

    public static void NextStage()
    {
        if (currentStage < amountOfStages)
        {
            stagesDone[currentStage] = true;
            SceneManager.LoadScene(stagePrefix + (++currentStage));
            Debug.Log("load stage" + currentStage);
        }
        else
        {
            SceneManager.LoadScene("GameOver");
            Debug.Log("GameOver");
        }
    }

    public static void LoadStageById(int stage)
    {
        SceneManager.LoadScene(stagePrefix + stage);
        currentStage = stage;
    }

    public static bool StageDone(int stage)
    {
        return stagesDone[stage - 1];
    }
}
