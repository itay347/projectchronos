﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TextureBehavior
{
    public Color color;
    public Sprite sprite;
}

public class TextureSwapper : BehaviorSwapper
{
    public TextureBehavior[] timeTexture = new TextureBehavior[2];
    // Start is called before the first frame update
    void Start()
    {
        foreach(TextureBehavior texture in timeTexture)
        {
            texture.color = new Color(texture.color.r, texture.color.g, texture.color.b);
        }
    }

    public override void PastSwitch()
    {
        setTexture(GetComponent<SpriteRenderer>(), timeTexture[(int)TimeLineZone.past]);
    }

    public override void PresentSwitch()
    {
        setTexture(GetComponent<SpriteRenderer>(), timeTexture[(int)TimeLineZone.present]);
    }

    private static void setTexture(SpriteRenderer toSet, TextureBehavior toUse)
    {
        toSet.color = toUse.color;
        toSet.sprite = toUse.sprite;
    }
}
