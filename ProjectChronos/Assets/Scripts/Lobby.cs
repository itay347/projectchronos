﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Lobby : MonoBehaviour
{
    public void StartGame()
    {
        StageManager.LoadStageById(1);
    }

    public void GoToStages()
    {
        SceneManager.LoadScene("Stages");
    }

    public void GoToCredits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void GoToLobby()
    {
        SceneManager.LoadScene("loby");
    }
}
