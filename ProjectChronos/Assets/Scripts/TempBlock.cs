﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempBlock : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (TimeLine.currentTimeLine== TimeLineZone.past && collision.gameObject.name == "Vases")
        {
            gameObject.GetComponent<VisibillitySwapper>().timeVisibillity[(int)TimeLineZone.present].visible = false;
            gameObject.GetComponent<TextureSwapper>().timeTexture[(int)TimeLineZone.present].sprite = null;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (TimeLine.currentTimeLine == TimeLineZone.past && collision.gameObject.name == "Vases")
        {
            gameObject.GetComponent<VisibillitySwapper>().timeVisibillity[(int)TimeLineZone.present].visible = true;
            gameObject.GetComponent<TextureSwapper>().timeTexture[(int)TimeLineZone.present].sprite =
                gameObject.GetComponent<TextureSwapper>().timeTexture[(int)TimeLineZone.past].sprite;
        }
    }
}
