﻿using UnityEngine;

public class cameraFollow : MonoBehaviour
{
    public float interpVelocity;
    public float minDistance;
    public float followDistance;
    public Transform target;
    public Vector3 offset;
    Vector3 targetPos;
    // Use this for initialization
    void Start()
    {
        targetPos = transform.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (target)
        {
            Vector3 posNoZ = transform.position;
            posNoZ.z = target.position.z;

            Vector3 targetDirection = target.position - posNoZ;

            interpVelocity = targetDirection.magnitude * 5f;

            targetPos = transform.position + (targetDirection.normalized * interpVelocity * Time.deltaTime);

            float transition = 0.25f;
            if (Mathf.Abs(transform.position.y - targetPos.y) > 0.9)
            {
                transition = 0.5f;
            }

            transform.position = Vector3.Lerp(transform.position, targetPos + offset, transition);

        }
    }
}
