﻿using System;
using UnityEngine;

namespace AssemblyCSharp
{
	public class AbstractKeyboard
	{
		private KeyCode w;
		private KeyCode s;
		private KeyCode d;
		private KeyCode a;
		private KeyCode jump;

		public AbstractKeyboard (KeyCode w, KeyCode s, KeyCode d, KeyCode a, KeyCode jump)
		{
			this.w = w;
			this.s = s;
			this.a = a;
			this.d = d;
			this.jump = jump;
		}

		public bool wDown() {
			return UnityEngine.Input.GetKey (this.w);
		}
		public bool sDown() {
			return UnityEngine.Input.GetKey (this.s);
		}
		public bool aDown() {
			return UnityEngine.Input.GetKey (this.a);
		}
		public bool dDown() {
			return UnityEngine.Input.GetKey (this.d);
		}
		public bool jumpDown() {
			return UnityEngine.Input.GetKey (this.jump);
		}
	}
}

