﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextStage : MonoBehaviour
{
    public bool finish;

    private void Start()
    {
        finish = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(!finish && collision.gameObject.tag == "Finish")
        {
            finish = true;
            StageManager.NextStage();
        }
    }
}
